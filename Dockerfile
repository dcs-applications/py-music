FROM python:3-alpine
MAINTAINER Dean Shanahan
RUN apk update
RUN apk add gcc py-pip 
RUN pip3 install --upgrade pip
COPY . .
WORKDIR .
RUN pip3 install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["start.py"]
